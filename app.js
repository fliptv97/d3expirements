var express = require( "express" );
var xls2json = require( "xls2json" );
var bodyParser = require( "body-parser" );
var app = express();

var port = 5050;

xls2json.convertFile( __dirname + "/data/AverageTime.xls", __dirname + "/public/data", function ( err, data ) {
  if (err) {
    console.log( err );
  } else {
    console.log( data );
  }
} );

app.use( bodyParser.json() );

app.use( express.static( __dirname + "/public" ) );

app.get( '/', function ( req, res ) {
  res.sendFile( __dirname + "/public/index.html" );
} );

app.get( "/data", function ( req, res ) {
  res.sendFile( __dirname + "/public/data/Sheet.json" );
} );

app.listen( port, function () {
  console.log( "Server listening on port", port );
} );
