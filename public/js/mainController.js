(function () {
  "use strict";

  angular
    .module( "demoChart" )
    .controller( "mainController", mainController );

  mainController.$inject = ["$scope", "$http"];

  function mainController( $scope, $http ) {
    var correctData = [];

    $scope.nodata_text = "No data available";

    $http
      .get( "http://localhost:5050/data" )
      .then( function ( response ) {
        correctData = [];
        $scope.data = response.data;

        for (var i = 0; i < $scope.data.length; i++) {
          correctData.push( {} );
          correctData[i].values = [];
          correctData[i].key = $scope.data[i].outage_date;

          var ind = 0;

          for (var t in $scope.data[i]) {
            if (t == 'outage_date') {
              continue;
            }
            correctData[i].values.push( {
              "x": t,
              "y": parseInt( $scope.data[i][t], 10 )
            } );
            ind++;
          }
        }

        $scope.data = correctData;
      }, function () {
        console.log( "Oops!" );
      } );

    $scope.options = {
      chart: {
        type: "multiBarChart",
        height: 276,
        margin: {
          top: 10,
          right: 70,
          bottom: 30,
          left: 70
        },
        x: function ( d ) {
          return d.x;
        },
        y: function ( d ) {
          return d.y;
        },
        stacked: true,
        showControls: false,
        showLegend: true,
        reduceXTicks: false,
        transitionDuration: 500,
        noData: $scope.nodata_text
      }
    };
  }
})();
