(function () {
  "use strict";

  angular
    .module( 'demoChart', ['nvd3'] );
})();
